###################
VTC System
###################

**RULES:**

- Take a card from Trello (https://trello.com/b/sXku7EZV/vtc) and add it to **In development** and assign your name to it

- As soon as you have finished a feature, commit, pull and then push your changes to the **develop** branch

- Move cards to **Testing** once finished and pushed

**HELP:**

- Database exports should be included in the *ROOT/DEV/*, the filename should be the date of the push.