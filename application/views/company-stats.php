<div class="col s12">
	<div class="row">
		<div class="col s12">
			<h1 class="page-title"><?php echo $title; ?></h1>
		</div>
	</div>

	<?php $this->load->view('blocks/links-dashboard-start-job.php'); ?>

	<?php
	if(count($jobs) > 0)
	{
	?>
		<div class="row stats-table">
			<div class="ets-container">
				<div class="row">
					<div class="col s12">
						<h3 class="box-title">
							Statistics
						</h3>
					</div>
				</div>

				<div class="row">
					<div class="col s12 m3">
						<div class="ets-container ets-dashboard-button">
							<h3 class="box-title">
								Total money
							</h3>
							<h2 class="stat">&pound;<?php echo number_format($total_company_money); ?></h2>
							<h5 class="sub-stat">Since <?php echo $time_of_first_job; ?></h5>
						</div>
					</div>

					<div class="col s12 m3">
						<div class="ets-container ets-dashboard-button">
							<h3 class="box-title">
								Total distance
							</h3>
							<h2 class="stat"><?php echo convert_units($total_company_distance,$user->setting_units,0).' '.$user->setting_units; ?></h2>
							<h5 class="sub-stat">Through <?php echo count($jobs); ?> jobs</h5>
						</div>
					</div>

					<div class="col s12 m3">
						<div class="ets-container ets-dashboard-button">
							<h3 class="box-title">
								Top earner
							</h3>
							<?php if($top_earner): ?>
								<h2 class="stat"><?php echo $top_earner['username']; ?></h2>
								<h5 class="sub-stat">&pound;<?php echo number_format($top_earner['total']); ?></h5>
							<?php else: ?>
								<h2 class="stat">N/A</h2>
								<h5 class="sub-stat">No jobs yet</h5>
							<?php endif; ?>
						</div>
					</div>

					<div class="col s12 m3">
						<div class="ets-container ets-dashboard-button">
							<h3 class="box-title">
								Top traveller
							</h3>
							<?php if($top_distance): ?>
								<h2 class="stat"><?php echo $top_distance['username']; ?></h2>
								<h5 class="sub-stat"><?php echo convert_units($top_distance['total'],$user->setting_units,0).' '.$user->setting_units; ?></h5>
							<?php else: ?>
								<h2 class="stat">N/A</h2>
								<h5 class="sub-stat">No jobs yet</h5>
							<?php endif; ?>
						</div>
					</div>
					
				</div>
			</div>
		</div>

		<div class="row stats-table">
			<div class="ets-container">
				<div class="row">
					<div class="col s12">
						<h3 class="box-title">
							Statistics
						</h3>
					</div>
				</div>
				<div class="row">
					
					<table class="dataTable">
						<thead>
							<tr>
								<th>Job ID</th>
								<th>Operator</th>
								<th>Job</th>
								<th>Started</th>
								<th>Distance (<?php echo $user->setting_units; ?>)</th>
								<th>Time</th>
								<th>Money</th>
								<th>Fuel economy</th>
								<?php if($rank && $rank[0]->view_notes == 1): ?>
									<th>Notes</th>
								<?php endif; ?>
							</tr>
						</thead>
						<tbody>
							<?php
							$stat_totalMoney = 0;
							$stat_totalDamage = 0;
							$stat_totalDistance = 0;
							$stat_totalTime = 0;
							?>
							<?php foreach($jobs as $job):?>
								<tr>
									<td><?php echo $job->journeyID; ?></td>
									<td><?php echo $this->user->get_user($job->userID)->username; ?></td>
									<td><strong><?php echo $job->start_point; ?></strong> to <strong><?php echo $job->end_point; ?></strong></td>
									<td><?php echo date('d/m/y g:ia',$job->time_added); ?></td>
									<td><?php echo convert_units($job->distance,$user->setting_units,0).' '.$user->setting_units; ?></td>
									<?php if(gmdate('G',$job->time_taken) == 0): ?>
										<td><?php echo gmdate('i \m\i\n\s',$job->time_taken); ?></td>
									<?php else: ?>
										<td><?php echo gmdate('G\h\r i \m\i\n\s',$job->time_taken); ?></td>
									<?php endif; ?>
									<td>&pound;<?php echo number_format($job->money); ?></td>
									<td><small>Coming Soon</small></td>
									<?php if($rank && $rank[0]->view_notes == 1): ?>
										<td>Notes</td>
									<?php endif; ?>
								</tr>
								<?php
								$stat_totalMoney = $stat_totalMoney + $job->distance;
								$stat_totalDamage = $stat_totalDamage + $job->damage;
								$stat_totalDistance = $stat_totalDistance + $job->distance;
								$stat_totalTime = $stat_totalTime + $job->time_taken;
								?>
							<?php endforeach; ?>
						</tbody>
					</table>
					
					<div class="row">
						<div class="col s12">
							<h3 class="box-title">
								Money earned over time
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m4 center">
							<span class="legend money"></span> Money
						</div>
						<div class="col s12 m4 center">
							<span class="legend distance"></span> Distance (<?php echo $user->setting_units; ?>)
						</div>
						<div class="col s12 m4 center">
							<span class="legend time"></span> Time taken (seconds)
						</div>
					</div>
					<div class="row">
						<div class="col s12 m6">
							<div class="ct-chart"></div>
						</div>
						<div class="col s12 m6">
							<div class="ct-pie-chart"></div>
						</div>
					<script>
							var data = {
							  	// A labels array that can contain any sort of values
							  	labels:
							  	[
							  		<?php foreach($jobs as $job): ?>
										'<?php echo $this->user->get_user($job->userID)->username; ?> (<?php echo date('d/m/y',$job->time_added); ?>)',
									<?php endforeach; ?>
							  	],
							  	// Our series array that contains series objects or in this case series data arrays
							  	series: 
							  	[
							    	[
							    		<?php foreach($jobs as $job): ?>
											<?php echo $job->money; ?>,
										<?php endforeach; ?>
									],
									[
							    		<?php foreach($jobs as $job): ?>
											<?php echo convert_units($job->distance,$user->setting_units,0); ?>,
										<?php endforeach; ?>
									],
									[
							    		<?php foreach($jobs as $job): ?>
											<?php echo $job->time_taken; ?>,
										<?php endforeach; ?>
									]
							  	]
							};

							var options = {
							    width: '100%',
							    height: "400px",
							    stackBars: false,
							    horizontalBars: false
							};

							// Create a new line chart object where as first parameter we pass in a selector
							// that is resolving to our chart container element. The Second parameter
							// is the actual data object.
							new Chartist.Bar('.ct-chart',data,options);


							// Pie chart for totals
							var options = {
							    width: '100%',
							    height: "400px",
							    stackBars: false,
							    horizontalBars: false
							};
							new Chartist.Pie('.ct-pie-chart',
							{
								series:
								[
									<?php echo $stat_totalMoney.','; ?>
									<?php echo $stat_totalTime.','; ?>
									<?php echo $stat_totalDistance.','; ?>
								],
							  	labels:
							  	[
							  		'Money',
							  		'Time',
							  		'Distance'
							  	]
							},
							{
								donut: true,
							  	showLabel: true,
							  	width:'100%',
							  	height:'400px'
							});
					</script>
				</div>
			</div>
		</div>
	<?php
	}
	else
	{
	?>
		<div class="row stats-table">
			<div class="ets-container">
				<div class="row">
					<div class="col s12">
						<h3 class="box-title">
							Statistics
						</h3>
					</div>
				</div>

				<div class="row">
					<div class="col s12 center">
						<h6>There are currently no jobs to report about!</h6>
					</div>
				</div>
			</div>
		</div>
	<?php
	}
	?>

</div>