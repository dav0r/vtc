<div class="row">
	<div class="col s12">
		<a class="ets-button back margin-right" href="<?php echo base_url('dashboard'); ?>"><i class="material-icons">&#xE314;</i> Back to dashboard</a>
		<a class="ets-button" href="<?php echo base_url('start-job'); ?>">Start a job</a>
	</div>
</div>