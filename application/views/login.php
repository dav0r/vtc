<div class="col s12">
	
	<div class="row">
		<div class="col s12">
			<h1 class="page-title"><?php echo $title; ?></h1>
		</div>
	</div>
	<div class="row journey-details">
		<div class="ets-container">
			<div class="row">
				<div class="col s12">
					<h3 class="box-title">
						Authentication
					</h3>
				</div>
			</div>
			<?php show_messages(); ?>
			<?php echo form_open("login"); ?>
				<div class="row">
					<div class="input-field col s6">
						<input type="text" id="username" name="username">
						<label for="username"><i class="material-icons">&#xE55A;</i> Username:</label>
				  	</div>
				  	<div class="input-field col s6">
						<input type="password" id="password" name="password">
						<label for="password"><i class="material-icons">&#xE897;</i> Password:</label>
					</div>
			  	</div>

				<div class="row">
					<div class="input-field col s12 m6">
						<a class="ets-button margin-right">Forgotten password</a><a href="<?php echo base_url('/register/user'); ?>" class="ets-button">Create account</a>
					</div>
					<div class="input-field col s12 m6 right-align">
						<input type="submit" value="Login" name="submitted"> 
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>