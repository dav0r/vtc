<div class="row">
	<div class="col s12">
		<h1 class="page-title"><?php echo $title; ?></h1>
	</div>
</div>
<div class="row register">
	<div class="ets-container">
		<div class="row">
			<div class="col s12">
				<h3 class="box-title">
					Driver Registration
				</h3>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<?php echo $this->session->flashdata('message'); ?>

				<?php echo form_open('register/user'); ?>

					<div class="input-field col s12">
						<?php echo form_input(array('type'=>'text','name'=>'username','value'=>$this->input->post('username'))); ?>
						<?php echo form_label('Choose your username','username'); ?>
					</div>
					<div class="input-field col s12">
						<?php echo form_input(array('type'=>'password','name'=>'password','value'=>$this->input->post('username'))); ?>
						<?php echo form_label('Password','password'); ?>
					</div>
					<div class="input-field col s12">
						<?php echo form_input(array('type'=>'password','name'=>'password_confirm','value'=>$this->input->post('username'))); ?>
						<?php echo form_label('Confirm password','password_confirm'); ?>
					</div>
					<div class="input-field col s12">
						<?php echo form_submit('submitted','Register'); ?>
					</div>

				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>