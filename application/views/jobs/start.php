<div class="col s12">
	<div class="row">
		<div class="col s12">
			<h1 class="page-title"><?php echo $title; ?></h1>
		</div>
	</div>

	<?php $this->load->view('blocks/links-dashboard.php'); ?>

	<div class="row journey-details">
		<div class="ets-container">
			<div class="row">
				<div class="col s12">
					<h3 class="box-title">
						Start a job
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					<input type="text" id="tags_start" name="start">
					<label for="tags_start"><i class="material-icons">&#xE52D;</i> Depart from:</label>
			  	</div>
			  	<div class="input-field col s12 m6">
					<input type="text" id="tags_finish" name="destination">
					<label for="tags_finish"><i class="material-icons">&#xE567;</i> Destination:</label>
				</div>
		  	</div>

		  	<div class="row">
				<div class="input-field col s12 m6">
					<input type="text" id="tags_start" name="start">
					<label for="tags_start"><i class="material-icons">&#xE52D;</i> Sold from:</label>
			  	</div>
			  	<div class="input-field col s12 m6">
					<input type="text" id="tags_finish" name="destination">
					<label for="tags_finish"><i class="material-icons">&#xE567;</i> Purchased by:</label>
				</div>
		  	</div>

		  	<div class="row">
				<div class="input-field col s12">
					<input type="text" id="tags_start" name="start">
					<label for="tags_start"><i class="material-icons">&#xE52D;</i> Distance (<?php echo $user->setting_units; ?>):</label>
			  	</div>
		  	</div>

			<div class="row">
				<div class="input-field col s12 m6">
					<input type="text" id="cargo" name="cargo">
					<label for="cargo"><i class="material-icons">&#xE8F1;</i> Cargo:</label>
				</div>
				<div class="input-field col s12 m6">
					<input type="text" id="cargo_class" name="cargo_class">
					<label for="cargo_class"><i class="material-icons">&#xE8F1;</i> Cargo class:</label>
				</div>
			</div>

		</div>
	</div>
</div>