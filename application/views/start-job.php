<div class="col s12">
	<div class="row">
		<div class="col s12">
			<h1 class="page-title"><?php echo $title; ?></h1>
		</div>
	</div>

	<?php $this->load->view('blocks/links-dashboard.php'); ?>

	<div class="row journey-details">
		<div class="ets-container">
			<div class="row">
				<div class="col s12">
					<h3 class="box-title">
						Job details
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<input type="text" id="tags_start" name="start">
					<label for="tags_start"><i class="material-icons">&#xE52D;</i> Depart from:</label>
			  	</div>
			  	<div class="input-field col s6">
					<input type="text" id="tags_finish" name="destination">
					<label for="tags_finish"><i class="material-icons">&#xE567;</i> Destination:</label>
				</div>
		  	</div>

			<div class="row">
				<div class="input-field col s12">
					<input type="text" id="cargo" name="cargo">
					<label for="cargo"><i class="material-icons">&#xE8F1;</i> Cargo:</label>
				</div>
			</div>

			<?php if($company->setting_mode == "advanced"): ?>
				<div class="row">
					<div class="col s12">
						<div class="info">
							Your manager has chosen to require as much information about the delivery as possible - <strong>this feature is coming soon!</strong> and will require you to provide the following information:
							<ul style="margin-left:20px;">
								<li>Load weight</li>
								<li>Load class</li>
								<li>Delivery characteristics (urgent, fragile etc.)</li>
								<li>Company selling goods</li>
								<li>Company who has purchased the goods</li>
							</ul>
							<p>You will receive additional XP based on the properties of your load</p>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<!-- Hidden fields -->
			<input type="hidden" name="time_added" value="">
			<input type="hidden" name="time_estimate" value="">
			<input type="hidden" name="time_taken" value="">
			<input type="hidden" name="time_taken_seconds" value="0">
			<input type="hidden" name="completed" value="">
			<input type="hidden" name="distance" value="">
			<input type="hidden" name="notes" value="">
			<input type="hidden" name="approved" value="">
			<input type="hidden" name="submission_url" value="<?php echo base_url('dashboard/add_job_ajax'); ?>">
			<input type="hidden" name="submission_confirm_url" value="<?php echo base_url('dashboard/complete_job'); ?>">

			<div class="row">
				<div class="input-field col s12">
					<input type="submit" id="calculate_route" value="Calculate route"> 
				</div>
			</div>
		</div>
	</div>

	<div class="row journey-overview">
		<div class="ets-container">
			<div class="row">
				<div class="col s12">
					<h3 class="box-title">
						Journey Overview
					</h3>
				</div>
			</div>

			<div class="row">
				<div class="col s12 m6">
					<div class="ets-container">
						<h3 class="box-title">Estimated distance:</h3>
						<div class="estimatedKm center-align" style="font-size:28px"></div>
					</div>
				</div>
				<div class="col s12 m6">
					<div class="ets-container">
						<h3 class="box-title">Estimated time:</h3>
						<div class="estimatedTime center-align" style="font-size:28px;"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<div id="map">

					</div>
				</div>
			</div>

			<div class="row timer-container">
				<div class="col s12 m12 center-align">
					<div class="ets-container">
						<h3 class="box-title">Tacograph</h3>
						<div class="timer">
							00:00:00
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s6 left-align">
					<a class="ets-button edit-journey" href="<?php echo base_url('start-job'); ?>">Edit details</a>
				</div>
				<div class="col s6 right-align">
					<!--<input type="submit" name="submit_job" value="Start job">-->
					<div class="ets-button start-journey">Start</div>
					<div class="ets-button cancel-journey hide margin-right">Cancel job</div>
					<div class="ets-button finish-journey hide">I have completed this job</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Structure -->
<div id="confirm-job" class="modal">
    <div class="modal-content">
      	<h4>Complete job</h4>
      	<p>Well done! Please check the information below is correct and add any notes as required (e.g. damage, job rating etc.)</p>

      	<form name="complete_job" method="post" action="<?php echo base_url('dashboard/finish_job'); ?>">
      		<input type="hidden" name="journeyID" value="0">
      		<input type="hidden" name="time_taken" value="0">
	      	<table>
		        <tbody>
		          	<tr>
		            	<th>Journey</th>
		            	<td>From <span class="confirm_start_point"></span> to <span class="confirm_end_point"></span></td>
		          	</tr>
		          	<tr>
		            	<th>Company</th>
		            	<td><strong><?php echo $company->name; ?></strong></td>
		          	</tr>
		          	<tr>
		            	<th>Cargo</th>
		            	<td><span class="confirm_cargo"></span></td>
		          	</tr>
		          	<tr>
		            	<th>Distance</th>
		            	<td><span class="confirm_distance"></span></td>
		          	</tr>
		          	<tr>
		            	<th>Time taken</th>
		            	<td><span class="confirm_time_taken"></span> <span class="in-time hide text-green"><i class="material-icons">&#xE8DC;</i> EARLY</span><span class="on-time hide text-green"><i class="material-icons">&#xE8DC;</i> ON TIME</span><span class="late hide text-green"><i class="material-icons">&#xE8DB;</i> LATE</span></td>
		          	</tr>
		          	<tr>
		            	<th>Fuel consumed</th>
		            	<td><input type="text" name="fuel" value="0" placeholder="Your total fuel consumption"></td>
		          	</tr>
		          	<tr>
		            	<th>Damage</th>
		            	<td><input type="text" name="damage" value="0" placeholder="The damage received"></td>
		          	</tr>
		          	<tr>
		            	<th>Money received</th>
		            	<td><input type="text" name="money" value="0" placeholder="Total money received after deductions"></td>
		          	</tr>
		          	<tr>
		            	<th>Notes</th>
		            	<td><textarea id="journey-notes" placeholder="Please enter any damage, accident reports and/or other relevant information" name="notes"></textarea></td>
		          	</tr>
		        </tbody>
	      	</table>
	      	<input type="submit" name="submitted" value="Save completed job" class="ets-button margin-top" id="confirm-complete-job">
	    </form>
    </div>
</div>

<script>
  	var map;
  	function initMap()
  	{
  		var directionsService = new google.maps.DirectionsService;
			var directionsDisplay = new google.maps.DirectionsRenderer;

  		var geocoder = new google.maps.Geocoder();
    	map = new google.maps.Map(document.getElementById('map'), {
      		center: {lat: 52.509535, lng: 8.371582},
      		zoom: 5
    	});
    	map.set('styles',[{"featureType":"landscape.natural","elementType":"all","stylers":[{"color":"#5959b3"}]},{"featureType":"all","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":0.5}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#150f0f"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"on"},{"invert_lightness":true},{"color":"#ebd17d"},{"weight":0.16}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#29292b"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#9e9245"}]},{"featureType":"road.highway.controlled_access","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"on"},{"color":"#000000"}]}]
    	);

    	document.getElementById('calculate_route').addEventListener('click', function() {
			geocodeAddress_start(geocoder, map);
			geocodeAddress_finish(geocoder, map);
			calculateAndDisplayRoute(directionsService, directionsDisplay);
	  	});

    	function geocodeAddress_start(geocoder, resultsMap) {
		  var address = document.getElementById('tags_start').value;
		  geocoder.geocode({'address': address}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		      resultsMap.setCenter(results[0].geometry.location);
		      var marker = new google.maps.Marker({
		        map: resultsMap,
		        position: results[0].geometry.location
		      });
		    } else {
		      alert('Please provide a starting location!');
		    }
		  });
		}
		function geocodeAddress_finish(geocoder, resultsMap) {
		  var address = document.getElementById('tags_finish').value;
		  geocoder.geocode({'address': address}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		      resultsMap.setCenter(results[0].geometry.location);
		      var marker = new google.maps.Marker({
		        map: resultsMap,
		        position: results[0].geometry.location
		      });
		    } else {
		      alert('Please provide a destination!');
		    }
		  });
		}

		function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		  directionsService.route({
		    origin: document.getElementById('tags_start').value,
		    destination: document.getElementById('tags_finish').value,
		    travelMode: google.maps.TravelMode.DRIVING
		  }, function(response, status) {
		    if (status === google.maps.DirectionsStatus.OK) {
		      	$('.estimatedMiles').html(response.routes[0].legs[0].distance.text)

		      	// Gives us estimated distance in KM
		      	var km = response.routes[0].legs[0].distance.text;
	      	
		      	// Gives us ETA in seconds
		      	var estimatedTime_seconds = parseInt(response.routes[0].legs[0].duration.value);

		      	var estimatedTime_seconds_game = estimatedTime_seconds / 8;

		      	// Gives us ETA in minutes
		      	var time = response.routes[0].legs[0].duration.value / 60;

		      	// Gives us ETA in hours
		      	var estimatedTime = (time / 60) / 5;

		      	// Split up the ETA so we can caluclate something that makes more sense to humans
		      	estimatedTime = estimatedTime.toString().split(".");

		      	// Work out minutes and hours seperately 
		      	var estimatedTime_minutes = parseFloat('0.'+estimatedTime[1].substring(0,2));
		      	var estimatedTime_hours = estimatedTime[0];

		      	// Save the human readable time ([x] hrs [y] minutes)
		      	estimatedTime = estimatedTime_hours+' hrs '+parseInt(estimatedTime_minutes * 60)+' minutes';

		      	$('.estimatedKm').html(km);
		      	$('.estimatedTime').html(estimatedTime);

		      	// Populate hidden journey fields
		      	$('input[name="time_added"]').val(new Date().getTime() / 1000);
		      	$('input[name="time_estimate"]').val(estimatedTime_seconds_game);
		      	$('input[name="time_taken"]').val('0');
		      	$('input[name="completed"]').val('0');
		      	$('input[name="cargo"]').val($('#cargo').val());
		      	$('input[name="distance"]').val(km.replace(/\D/g,''));
		      	$('input[name="notes"]').val('none');
		      	$('input[name="approved"]').val('0');		

		      	$('.journey-overview').slideDown('slow',function() {
		      		google.maps.event.trigger(map, "resize");
		      		directionsDisplay.setMap(map);
		      		$('html, body').animate({
						scrollTop: $(".journey-overview").offset().top
					}, 1000);

		      	});

		      	$('.edit-journey').click(function() {
		      		$('.journey-overview').slideUp('slow');

		      		$('html, body').animate({
						scrollTop: $("body").offset().top
					}, 1000);
		      	});

		      	// console.log('Estimated time in seconds: '+ estimatedTime_seconds);
		      	// console.log('Estimated miles: '+miles);
		      	// console.log('Estimated game time: '+estimatedTime);

		      	// Formula to calc XP:
		      	// Time in seconds divided by 2 + mileage * 0.5;

		      	//var point = response.routes[0].legs[0];
		      	//console.log(point.duration);
		      	// console.log(response.getDuration().seconds+" seconds");

		      	//console.log(response.routes[0].legs[0].distance.text);
		      directionsDisplay.setDirections(response);
		    } else {
		      //window.alert('Directions request failed due to ' + status);
		    }
		  });
		}
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_npRM6FP7Qf4CIBzz4GhwQXlUQiepuSo&callback=initMap" async defer></script>