<div class="col s12">
	<div class="row">
		<div class="col s12">
			<h1 class="page-title"><?php echo $title; ?></h1>
		</div>
	</div>

	<div class="row">
		<div class="ets-container">
			<div class="row">
				<div class="col s12">
					<h3 class="box-title">
						Welcome back <?php echo $username; ?>
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<?php show_messages(); ?>
				</div>
			</div>
			<div class="row">
				<?php if($company): ?>
					<div class="col s12 m3">
						<a href="<?php echo base_url('dashboard/start-job'); ?>">
							<div class="ets-container ets-dashboard-button">
								<h3 class="box-title">
									Start a job
								</h3>
								<i class="material-icons">&#xE422;</i>
							</div>
						</a>
					</div>
					<div class="col s12 m3">
						<a href="<?php echo base_url('company-statistics/'.$company[0]->companyID); ?>">
							<div class="ets-container ets-dashboard-button">
								<h3 class="box-title">
									Company statistics
								</h3>
								<i class="material-icons">&#xE8E1;</i>
							</div>
						</a>
					</div>
				<?php else: ?>
					<div class="col s12 m3">
						<a href="<?php echo base_url('company/register'); ?>">
							<div class="ets-container ets-dashboard-button">
								<h3 class="box-title">
									Create a company
								</h3>
								<i class="material-icons">&#xE148;</i>
							</div>
						</a>
					</div>
					<div class="col s12 m3">
						<a href="<?php echo base_url('company/join'); ?>">
							<div class="ets-container ets-dashboard-button">
								<h3 class="box-title">
									Join a company
								</h3>
								<i class="material-icons">&#xE252;</i>
							</div>
						</a>
					</div>
				<?php endif; ?>
				<div class="col s12 m3">
					<div class="ets-container ets-dashboard-button disabled">
						<h3 class="box-title">
							My Statistics
						</h3>
						<i class="material-icons">&#xE6E1;</i>
					</div>
				</div>
				<div class="col s12 m3">
					<div class="ets-container ets-dashboard-button disabled">
						<h3 class="box-title">
							Messages
						</h3>
						<i class="material-icons">&#xE0C9;</i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php //if($xp): ?>
		<div class="row" id="experience">
			<div class="ets-container">
				<div class="row">
					<div class="col s12">
						<h3 class="box-title">
							Driver Progress <span><?php echo $xp['alias']; ?></span> <small>(<?php echo $xp['current_xp']; ?>/<?php echo $xp['next_level_xp']; ?>)</small>
						</h3>
					</div>
				</div>
				<div class="row">					
					<div class="col s12">
						<div class="row xp-bar-container">
							<div class="col s4 m1 xp-current-level">
								<h5><span>Level</span><?php echo $xp['current_level']; ?></h5>
							</div>
							<?php
							// Calculate percentage width for progress bar
							$width = ($xp['current_xp'] / $xp['next_level_xp']) * 100;
							?>
							<div class="col s4 m10">
								<div class="xp-progress-container">
									<div class="xp-progress" data-animatewidth="<?php echo $width; ?>">
										
										<div class="marker">
											<?php echo $xp['current_xp']; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col s4 m1 xp-next-level">
								<h5><span>Level</span><?php echo $xp['next_level']; ?></h5>
							</div>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col s12 m2">
						<div class="achievement active">
							<i class="material-icons">&#xE55B;</i>
							<span>Explorer</span>
							<div class="description tip">
								<p>Travel</p>
							</div>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement active">
							<i class="material-icons">&#xE8D0;</i>
							<span>CEO</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE227;</i>
							<span>Bread Winner</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE01B;</i>
							<span>Fashionably late</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE87E;</i>
							<span>Helpful</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE8AF;</i>
							<span>Chatterbox</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE1A3;</i>
							<span>Green Energy</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE3DA;</i>
							<span>No time to chat</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE318;</i>
							<span>Promotion</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE324;</i>
							<span>Always on the move</span>
							<div class="description tip">
								<p><strong>Always on the move:</strong></p>
								<p>Track your journey progress from a mobile device</p>
							</div>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE438;</i>
							<span>Lookin' good</span>
						</div>
					</div>
					<div class="col s12 m2">
						<div class="achievement">
							<i class="material-icons">&#xE54F;</i>
							<span>Relaxed</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php //endif; ?>
</div>