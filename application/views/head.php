<!DOCTYPE html>
  	<html>
    	<head>
      		<!--Import Google Icon Font-->
      		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      		<!--Import materialize.css-->
      		<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/materialize.min.css'); ?>"  media="screen,projection"/>
      		<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>"  media="screen,projection"/>
          <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>">
          <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
          <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
          <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
          <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
          <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

      		<!--Let browser know website is optimized for mobile-->
      		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    	</head>

    	<body>

    		<div class="container">
	    		<div class="top bar">
	      			Jade &amp; Dave Logistics
	  				<ul id="nav-mobile" class="right hide-on-med-and-down">
              <li><a href="<?php echo base_url('/dashboard'); ?>">Dashboard</a></li>
	        			<?php //if($this->user->checklogin()): ?>
                  <li><a href="<?php echo base_url('logout'); ?>">Logout</a></li>
                <?php //else: ?>
                  <li><a href="<?php echo base_url('login'); ?>">Login</a></li>
                <?php //endif; ?>
	      			</ul>
	    		</div>
	    	</div>

	  		<div class="container">

	  			<div class="row">

