<?php

// Convert input paramter (km) into desired unit
// $km = Kilometers input
// $result = Unit of measurement to output ('km','miles','feet','yards')
// $dp = Decimal places to return (default: 2)
function convert_units($km,$result="km",$dp=false)
{
	if(is_numeric($km))
	{
		switch ($result)
		{
		    case "km":
		        // No need to change anything
		    	$return = $km;
		        break;

		    case "mi":
		        // Convert KMs into MIs
		    	$return = $km * 0.621371;
		        break;

		    case "ft":
		        // Convert KMs into FT
		    	$return = $km * 3280.84;
		        break;

		    case "yds":
		    	// Conert KM into YDS
		    	$return = $km * 1093.61;
		    	break;

		    default:
		    	return false;
		}

		if(is_numeric($dp))
		{
			return number_format($return,$dp);
		}
		else
		{
			return $return;
		}
	}
	else
	{
		return false;
	}
}

function convert_currenct()
{
	
}