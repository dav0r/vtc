<?php

Class Jobs extends CI_Controller
{

	public function index()
	{

	}

	public function start()
	{
		if($this->user->checklogin())
		{
			$data['title'] = "Start a job";

			// User is logged in, get user data
			$data['user'] = $this->user->get_user();

			// Check that they're in a company
			$data['company'] = $this->company->get_company_by_user($data['user']->userID);

			if($data['company'])
			{
				// Job has been submitted for starting
				if($this->input->post('start-job'))
				{

				}

				// Job has been finished
				if($this->input->post('finish-job'))
				{

				}

				// Load view
				$this->load->view('head');
				$this->load->view('jobs/start',$data);
				$this->load->view('foot');
			}
			else
			{
				// They're not in a company, return to dashboard with an error
				$this->session->set_flashdata('message','<div class="error"><p>You need to be in or own a company before starting a job!</p></div>');
				redirect("/dashboard");
			}

		}
		else
		{
			die("Not logged in apparently...");
			$this->session->set_flashdata('message','<div class="error"><p>You need to be logged in to access that page.</p></div>');
			$this->session->set_userdata('redirect',base_url('jobs/start'));
			redirect("/login");
		}
	}

	public function removejob($jobID=null)
	{
		echo '<p>Coming soon</p>';
	}

	public function editjob($jobID=null)
	{
		echo '<p>Coming soon</p>';
	}

}