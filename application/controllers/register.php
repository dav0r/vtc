<?php

Class Register extends CI_Controller
{

	public function user()
	{
		if(checklogin())
		{
			redirect("/dashboard");
		}

		// Title
		$data['title'] = "Driver registration";

		// Check if form has been submitted
		if($this->input->post('submitted'))
		{
			$errors = array();
			if(empty($this->input->post('username')))
			{
				$errors[] = "<li>Please enter your username</li>";
			}
			if(empty($this->input->post('password')))
			{
				$errors[] = "<li>Please enter your password</li>";
			}
			else
			{
				if(strlen($this->input->post('password')) < 8)
				{
					$errors[] = "<li>Your password must be at least 8 characters long</li>";
				}
				else
				{
					// Check that the password has been confirmed
					if(empty($this->input->post('password_confirm')))
					{
						$errors[] = "<li>Please confirm your password</li>";
					}
					else
					{
						// Both have been filled in, check that they match
						if($this->input->post('password') != $this->input->post('password_confirm'))
						{
							$errors[] = "<li>Your passwords did not match</li>";
						}
					}
				}
			}
			
			// Load registration model
			$this->load->model('Register_Model');

			// Check that the username doesn't exist already
			$username_check = $this->Register_Model->register_user_check();

			if($username_check > 0)
			{
				$errors[] = "<li>Your username already exists</li>";
			}

			// Check for errors
			if(empty($errors))
			{
				// Everything okay, carry on.
				$register = $this->Register_Model->register_user();

				if($register)
				{
					// User registered
					$this->session->set_userdata('userID',$register);
					$this->session->set_userdata('loggedin',true);

					redirect("/dashboard");
				}
				else
				{
					$this->session->set_flashdata('message','<div class="error"><p>There was a problem creating your account, please try again.</p></div>');
				}
			}
			else
			{
				// Build up errors message
				$message = '';
				foreach($errors as $msg)
				{
					$message .= '<li>'.$msg.'</li>';
				}
				$this->session->set_flashdata('message','<div class="error"><ul>'.$message.'</ul></div>');
			}

		}

		// Load view
		$this->load->view('head');
		$this->load->view('register',$data);
		$this->load->view('foot');
	}

	public function company()
	{

	}

}