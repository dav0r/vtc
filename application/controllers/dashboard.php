<?php

Class Dashboard extends CI_Controller
{

	public function index()
	{
		// Check if they're logged in
		if($this->user->checklogin())
		{
			// Get user data
			$data['user'] = $this->user->get_user($this->session->userdata('userID'));

			// Check if they're in a company and assign it.
			$this->load->model('Company_Model');
			$company = $this->Company_Model->get_company_by_user($data['user']->userID);

			// If they're in a company
			if($company)
			{
				// Get company information
				$data['company'] = $this->Company_Model->get_company($company[0]->companyID);
			}
			else
			{
				// Otherwise, nothing.
				$data['company'] = null;
			}

			// Get level & XP
			$data['xp'] = $this->experience->summary($data['user']->xp);

			// Set view vars
			$data['title'] = "My dashboard";
			$data['username'] = $data['user']->username;

			// Load views
			$this->load->view('head');
			$this->load->view('dashboard',$data);
			$this->load->view('foot');
		}
		else
		{
			// User isn't logged in, so set message and redirect to login.
			$this->session->set_flashdata('message','<div class="info">You need to be logged in to access this page.</div>');
			redirect("/login");
		}
	}

	public function start_job()
	{
		if($this->user->checklogin())
		{
			// Get user data
			$data['user'] = $this->user->get_user($this->session->userdata('userID'));

			// Check if they're in a company
			$this->load->model('company');
			$data['company']['employee'] = $this->company->get_company_by_user($data['user']->userID)[0];
			$data['company'] = $this->company->get_company($data['company']['employee']->companyID)[0];

			if($data['company'])
			{
				// Do funky stuff
				$data['title'] = "Start a job";

				$this->load->view('head');
				$this->load->view('start-job',$data);
				$this->load->view('foot');
			}
			else
			{
				$this->session->set_flashdata('message','<div class="error">You need to be a member of a company before starting a job.</div>');
				redirect("/dashboard");
			}
		}
		else
		{
			$this->session->set_flashdata('message','<div class="info">You need to be logged in to access this page.</div>');
			redirect("/login");
		}
	}

	public function add_job_ajax()
	{
		if($this->user->checklogin())
		{
			// Login okay!
			// Get user data
			//$this->load->model('user_model');
			//$user = $this->user_model->get_user($this->session->userdata('userID'));
			$user = $this->user->get_user();

			// Check if they're in a company
			$this->load->model('company');
			$company = $this->company->get_company_by_user($user->userID);

			if($company)
			{
				// Check form
				if($this->input->post('startjob'))
				{
					// Load journey model
					$this->load->model('Journey_Model');
					$add_job = $this->Journey_Model->add_journey($company[0]->companyID);

					echo $add_job;
				}
				else
				{
					echo "no-data";
				}
			}
			else
			{
				echo "no-company";
			}
		}
		else
		{
			// Not logged in!
			echo "no-login";
		}
	}

	public function finish_job()
	{
		if($this->user->checklogin())
		{
			// Login okay!
			// Get user data
			$user = $this->user->get_user($this->session->userdata('userID'));

			// Check if they're in a company
			// $this->load->model('company');
			// $company = $this->company->get_user($user->userID);
			$this->load->model('company');
			$data['employee'] = $this->company->get_company_by_user($user->userID)[0];
			$data['company'] = $this->company->get_company($data['employee']->companyID)[0];

			if($data['employee'] && $data['company'])
			{
				// Check form
				if($this->input->post('submitted'))
				{

					// Calculate XP
					// Need to load already saved info about the journey first
					$this->load->model('Journey_Model');
					$journey = $this->Journey_Model->get_journey($this->input->post('journeyID'))[0];

					// Calculate it based on the values given
					$xp = $this->experience->generate(
						$journey->distance,
						$this->input->post('time_taken'),
						$journey->time_estimate,
						$this->input->post('money'),
						$this->input->post('fuel'),
						$this->input->post('damage')
					);
					
					// Load journey model
					$this->load->model('Journey_Model');
					$update_job = $this->Journey_Model->update_journey();

					if($update_job == true)
					{
						// Job has been updated, so update the players XP
						$update_xp = $this->user_model->update_xp($user->xp + $xp,$user->userID);
						echo '<script>alert("Should be adding: '.$xp.'")</script>';
						echo '<script>alert("New total: '.$update_xp.'")</script>';

						if($update_xp == true)
						{
							// Set some flashdata so we can show some fancy animation on the dashboard
							$this->session->set_flashdata('xp',$xp);
						}

						$this->session->set_flashdata('message','<div class="success">Thank you! Your job has been saved successfully.</div>');
					}
					else
					{
						$this->session->set_flashdata('message','<div class="error">We\'re really sorry, there was a problem saving your job. Do not worry, we should still have a record of your job, so please contact a member of staff quoting job number <strong>'.$this->input->post('journeyID').'</strong>!</div>');
					}
				}
				else
				{
					$this->session->set_flashdata('message','<div class="error">It looks like you have tried to complete a job more than once.</div>');
				}
			}
			else
			{
				$this->session->set_flashdata('message','<div class="error">You need to be a member of a company to complete a job.</div>');
			}
		}
		else
		{
			// Not logged in!
			$this->session->set_flashdata('message','<div class="info">You must be logged in to access that page.</div>');
		}

		//die(print_r($data['employee']));
		//die("Your journey has been saved, above is a breakdown of how the XP was calculated");
		redirect("/dashboard#experience");
	}

}