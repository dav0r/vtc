<?php

Class Companies extends CI_Controller
{

	public function company_stats($companyID)
	{
		if($this->user->checklogin())
		{
			// Get user data
			// $this->load->model('user_model');
			// $user = $this->user_model->get_user($this->session->userdata('userID'));
			$data['user'] = $this->user->get_user();

			// Get rank
			$data['rank'] = $this->user_model->get_rank($data['user']->rank);

			if(empty($data['rank']))
			{
				// Assume they're the lowest rank
				$data['rank'] = null;
			}

			// Get all deliveries for this company
			$this->load->model('company');
			$data['jobs'] = $this->company->all_jobs($companyID);

			// Check if there's any journeys before proceeding
			if(count($data['jobs']) > 0)
			{
				// Get total money earned
				$this->load->model('company');
				$data['total_company_money'] = $this->company->get_earnings($companyID);

				// Get total distance traveled
				$data['total_company_distance'] = $this->company->get_distance($companyID);

				// Get top earner
				$top_earner = $this->company->get_best('money')[0];
				$data['top_earner'] = array("userID"=>$top_earner->userID,"username"=>$this->user->get_user($top_earner->userID)->username,"total"=>$top_earner->total);
				
				// Get furthest distance
				$top_distance = $this->company->get_best('distance')[0];
				$data['top_distance'] = array("userID"=>$top_distance->userID,"username"=>$this->user->get_user($top_distance->userID)->username,"total"=>$top_distance->total);

				// Get time of first job
				$data['time_of_first_job'] = date('d/m/Y',$this->company->get_first_job()[0]->time_added);

			}

			// Do funky stuff
			$data['title'] = "Company Statistics";
			$data['username'] = $data['user']->username;

			$this->load->view('head');
			$this->load->view('company-stats',$data);
			$this->load->view('foot');
		}
		else
		{
			$this->session->set_flashdata('message','<div class="info">You need to be logged in to access this page.</div>');
			redirect("/login");
		}
	}

}