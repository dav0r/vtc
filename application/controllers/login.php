<?php

Class Login extends CI_Controller
{

	public function index()
	{
		// Check if they're already logged in
		if($this->user->checklogin())
		{
			if($this->session->userdata('redirect'))
			{
				redirect($this->session->userdata('redirect'));
			}
			else
			{
				redirect("/dashboard");
			}
		}

		// Check if form has been submitted
		if($this->input->post('submitted'))
		{
			if($this->input->post('username') && $this->input->post('password'))
			{
				// Load the login model
				$this->load->model('user_model');

				$user = $this->user_model->login();

				if($user)
				{
					//$this->session->set_flashdata('message','<div class="success">Got user!</div>');

					$this->session->set_userdata('loggedin',true);
					$this->session->set_userdata('userID',$user[0]->userID);

					if($this->session->userdata('redirect'))
					{
						redirect($this->session->userdata('redirect'));
					}
					else
					{
						redirect("/dashboard");
					}
				}
				else
				{
					$this->session->set_flashdata('message','<div class="error">The details you entered are incorrect.</div>');
				}
			}
			else
			{
				// Set error message
				$this->session->set_flashdata('message','<div class="error">Please fill all fields in.</div>');
			}
		}

		$data['title'] = "Login";
		$this->load->view('head');
		$this->load->view('login',$data);
		$this->load->view('foot');
	}

	public function logout()
	{
		$this->session->unset_userdata('userID');
		$this->session->unset_userdata('loggedin');
		$this->session->set_flashdata('message','<div class="success">You have been logged out successfully.</div>');

		redirect("/login");
	}

}