<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class User {

    public function get_user($userID=null)
    {
    	$CI =& get_instance();

    	// If the userID isn't set, try and set it to the userID session (if it exists).
 		if(is_null($userID))
 		{
 			$userID = $CI->session->userdata('userID');
 		}

 		// Check for the userID again in case the session isn't set.
		if($userID)
		{
			// Check the database
			$CI->load->model('user_model');
			$checklogin = $CI->user_model->get_user($userID);

			if($checklogin)
			{
				return $checklogin[0];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
    }

    public function get_rank($userID=null)
    {
    	$CI =& get_instance();

    	// If the userID isn't set, try and set it to the userID session (if it exists).
 		if(is_null($userID))
 		{
 			$userID = $CI->session->userdata('userID');
 		}

 		// Check for the userID again in case the session isn't set.
		if($userID)
		{
			// Get the user's rank ID
			$rankID = $CI->user->get_user($userID)->rank;

			// Get the rank
			$CI->load->model('user_model');
			return $CI->user_model->get_rank($rankID);
		}
		else
		{
			return false;
		}
    }

    function checklogin($return_user_data=true)
	{
		$CI =& get_instance();

		if($CI->session->userdata('userID') && $CI->session->loggedin == true)
		{
			if($return_user_data==true)
			{
				// Check the database
				$CI->load->model('user_model');
				$checklogin = $CI->user_model->get_user($CI->session->userdata('userID'));

				if($checklogin)
				{
					return $checklogin;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

}