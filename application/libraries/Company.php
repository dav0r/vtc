<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Company {

    public function get_company_by_user($userID=null)
    {
    	$CI =& get_instance();

    	// If the userID isn't set, try and set it to the userID session (if it exists).
 		if(is_null($userID))
 		{
 			$userID = $CI->session->userdata('userID');
 		}

 		// Check for the userID again in case the session isn't set.
		if($userID)
		{
			// Setup return array
			$return = array();

			// Check the database
			$CI->load->model('Company_Model');
			$return['company_employee'] = $CI->Company_Model->get_company_by_user($userID);

			if($return['company_employee'])
			{
				// Get the company
				$return['company'] = $CI->Company_Model->get_company($return['company_employee'][0]->companyID);

				if($return['company'])
				{
					return $return;
				}
				else
				{
					// Failed getting company.
					return false;
				}
			}
			else
			{
				// Failed to get company employee
				return false;
			}
		}
		else
		{
			// User is not logged in properly.
			return false;
		}
    }

}