<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Experience {

	function summary($xp)
	{
		if(is_numeric($xp))
		{
			$CI =& get_instance();

			// Load the XP model
			$CI->load->model('Experience_Model');
			$gates = $CI->Experience_Model->get_gates();

			// Get summary
			$return = array();
			$i = 0;	
			foreach($gates as $gate)
			{
				if($xp > $gate->required_xp)
				{
					$return['alias'] = $gate->alias;
					$return['current_level'] = $gate->level;
					$return['current_xp'] = $xp;
					$return['next_level_xp'] = $gates[$i+1]->required_xp;
					$return['next_level'] = $gates[$i+1]->level;
					$return['next_level_xp_required'] = $gates[$i+1]->required_xp - $xp;
				}
				$i++;
			}

			return $return;
		}
		else
		{
			return false;
		}
	}

	function generate($distance,$time_taken,$time_estimate,$money,$fuel,$damage)
	{
		$return = '';
		// $return = $distance.'<br>';
		// $return .= $time_taken.'<br>';
		// $return .= $time_estimate.'<br>';
		// $return .= $money.'<br>';
		// $return .= $fuel.'<br>';
		// $return .= $damage;

		//$damage = 1000;

		//$time_taken = 2222;
		$xp = array();

		$xp_distance = $distance * 0.1;
		$return .= '
		<strong>XP Gained for distance <em>('.$distance.' km)</em>: <small>(distance * 0.1)</small></strong>
		'.$xp_distance.'
		';
		$xp[] = $xp_distance;

		$time_difference = $time_estimate - $time_taken;
		$xp_time = $time_difference * 0.1;
		$return .= '
		<strong>XP Gained for time:</strong>
		Time difference: '.$time_difference.' (estimated time ('.$time_estimate.') - time taken ('.$time_taken.'))
		'.$xp_time.' (time difference ('.$time_difference.') * 0.1)
		';
		$xp[] = $xp_time;

		if($money > 0)
		{
			$xp_money = $money * 0.1;
			$return .= '
			<strong>XP Gained for money earned <em>('.$money.')</em>: <small>(money * 0.1)</small></strong>
			'.$xp_money.'
			';
			$xp[] = $xp_money;
		}

		$xp_damage = ($damage * 2.1) * -1;
		$return .= '
		<strong>Damage XP <em>('.$damage.'%)</em>: <small>((damage ('.$damage.') * 2.1) * -1)</small></strong>
		'.$xp_damage.'
		';
		$xp[] = $xp_damage;

		$return .= '
		<strong>XP for fuel consumption is not considered</strong>
		';

		$return .= '<h3>Total XP:</h3>';
		$xp_positives = 0;
		$xp_negatives = 0;
		foreach($xp as $x)
		{
			if($x > 0)
			{
				$xp_positives = $xp_positives + $x;
			}
			else
			{
				$xp_negatives = $xp_negatives - $x;
			}
		}

		$total_xp = $xp_positives - $xp_negatives;

		if($total_xp < 0)
		{
			$total_xp = 0;
		}

		$return .= '<h1>'.$total_xp.'</h1>';

		return $total_xp / 3;
	}
}