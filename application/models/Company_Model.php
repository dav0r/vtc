<?php

Class Company_Model extends CI_Model
{

	// Retrieve company details that a user is part of
	public function get_company_by_user($userID)
	{
		$query = $this->db->select('*')->where('userID',$userID)->limit(1)->get('companies_users')->result();
		return $query;
	}

	// Get company information
	public function get_company($companyID)
	{
		$query = $this->db->select('*')->where('companyID',$companyID)->limit(1)->get('companies')->result();
		return $query;
	}

	// Get all jobs for this company
	public function all_jobs($companyID)
	{
		$query = $this->db->order_by('journeyID','ASC')->where(array('companyID'=>$companyID,'completed'=>'1'))->get('journeys')->result();
		return $query;
	}

	public function get_earnings($companyID)
	{
		$this->db->select('money')->where('completed','1')->where('companyID',$companyID);
		$query = $this->db->get('journeys')->result();
		$money = 0;
		
		foreach($query as $job)
		{
			$money = $money + $job->money;
		}

		return $money;
	}

	public function get_distance($companyID)
	{
		$this->db->select('distance')->where(array('completed'=>'1','companyID'=>$companyID));
		$query = $this->db->get('journeys')->result();
		$distance = 0;
		
		foreach($query as $job)
		{
			$distance = $distance + $job->distance;
		}

		return $distance;
	}

	public function get_best($field)
	{
		$qry = $this->db->query('SELECT userID,SUM('.$field.') as total from journeys Group By userID ORDER BY total DESC LIMIT 1');

		return $qry->result();
	}

	// public function get_user($userID)
	// {
	// 	$qry = $this->db->get()
	// 	$qry = $this->db->query('SELECT userID,SUM('.$field.') as total from journeys Group By userID ORDER BY total DESC LIMIT 1');

	// 	return $qry->result();
	// }

	public function get_first_job()
	{
		$this->db->select('time_added');
		$this->db->order_by('time_added','ASC');
		$this->db->limit(1);

		$qry = $this->db->get('journeys');
		return $qry->result();
	}

}