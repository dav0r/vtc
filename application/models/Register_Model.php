<?php

Class Register_Model extends CI_Model
{

	public function register_user()
	{
		$data = array(
   			'username' => $this->input->post('username'),
   			'password' => md5($this->input->post('password'))
		);
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

	public function register_user_check()
	{
		$this->db->select('userID');
		$this->db->from('users');
		$this->db->where('username', $this->input->post('username'));
		
		return $this->db->count_all_results();
	}

}