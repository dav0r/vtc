<?php

Class Journey_Model extends CI_Model
{

	public function add_journey($companyID)
	{

		$time_added = time();

		$data = array(
		   'userID' => $this->session->userdata('userID'),
		   'companyID' => $companyID,
		   'time_added' => $time_added,
		   'time_estimate' => $this->input->post('time_estimate'),
		   'time_taken' => $this->input->post('time_taken'),
		   'start_point' => $this->input->post('start_point'),
		   'end_point' => $this->input->post('end_point'),
		   'completed' => $this->input->post('completed'),
		   'cargo' => $this->input->post('cargo'),
		   'distance' => $this->input->post('distance'),
		   'notes' => $this->input->post('notes'),
		   'approved' => $this->input->post('approved'),
		);

		$this->db->insert('journeys', $data);
		return $this->db->insert_id();
	}

	public function update_journey()
	{
		$data = array(
               'time_taken' => $this->input->post('time_taken'),
               'completed' => '1',
               'notes' => $this->input->post('notes'),
               'damage' => $this->input->post('damage'),
               'fuel' => $this->input->post('fuel'),
               'money' => $this->input->post('money')
            );

		$this->db->where('journeyID', $this->input->post('journeyID'));
		$this->db->limit(1);
		$this->db->update('journeys', $data);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function get_journey($journeyID)
	{
		$this->db->where('journeyID', $journeyID);
		$this->db->limit(1);
		$query = $this->db->get('journeys');
		return $query->result();
	}
}