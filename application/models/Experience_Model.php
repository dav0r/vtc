<?php

Class Experience_Model extends CI_Model
{

	public function get_gates()
	{
		return $this->db->order_by('level','ASC')->get('xp_level_gates')->result();
	}

	public function get_level_range()
	{
		return $this->db->where('','')->get('xp_level_gates');
	}

}