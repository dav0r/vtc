<?php

Class User_Model extends CI_Model
{

	public function get_user($id)
	{
		$this->db->where('userID',$id);
		$this->db->limit(1);
		$user = $this->db->get('users');
		
		return $user->result();
	}

	public function get_rank($rankID)
	{
		return $this->db->select('*')->where('rankID',$rankID)->limit(1)->get('companies_ranks')->result();
	}

	public function update_xp($xp,$userID)
	{
		$this->db->set('xp',$xp);
		$this->db->where('userID',$userID);
		$this->db->limit(1);
		$this->db->update('users');

		if ($this->db->affected_rows() == '1')
		{
		    return true;
		}
		else
		{
		    return false;
		}
	}

	public function login()
	{
		$this->db->select('userID');
		$this->db->where('username',$this->input->post('username'));
		$this->db->where('password',md5($this->input->post('password')));
		$this->db->limit(1);
		$user = $this->db->get('users');

		return $user->result();
	}

}