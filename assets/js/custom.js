$(document).ready(function() {
	
	$('.dataTable').dataTable();

	$('.tooltipped').tooltip({delay: 50});

	$('.achievement').hover(function() {
		$('.tip',this).css('bottom','0');
	},function() {
		$('.tip',this).css('bottom','-100%');
	});

	// Animate xp bars
	$('.xp-progress').each(function() {
		$(this).css('width',$(this).data('animatewidth')+'%');
	});
	$('.marker').each(function () {
          $(this).prop('Counter',0).stop().animate({
              Counter: $(this).text()
          }, {
              duration: 3000,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              },
              complete: function() {
                counterActive = false;
              }
          });
        });

	$('.dashboard-button').hover(function() {
		$('.material-icons',this).stop().animate({'opacity':'0.05','margin-top':'20px'},150);
		$('h3',this).stop().animate({'margin-top':'-60px'},150);
		$('h5',this).stop().fadeIn(150);
	},function() {
		$('.material-icons',this).stop().animate({'opacity':'0.4','margin-top':'0px'},150);
		$('h3',this).stop().animate({'margin-top':'0px'},150);
		$('h5',this).stop().fadeOut(150);
	});
	var availableTags_ATS = [
      	"Bakersfield, California",
      	"Barstow, California",
      	"Carlsbad, California",
      	"El Centro, California",
      	"Eureka, California",
      	"Fresno, California",
      	"Hornbrook, California",
      	"Huron, California",
      	"Los Angeles, California",
      	"Oakdale, California",
      	"Oakland, California",
      	"Oxnard, California",
      	"Redding, California",
      	"Sacramento, California",
      	"San Diego, California",
      	"San Fransisco, California",
      	"San Rafael, California",
      	"Santa Cruz, California",
      	"Stockton, California",
      	"Truckee, California",

      	"Primm, Nevada",
      	"Las Vegas, Nevada",
      	"Tonopah, Nevada",
      	"Pioche, Nevada",
      	"Carson City, Nevada",
      	"Ely, Nevada",
      	"Reno, Nevada",
      	"Winnemucca, Nevada",
      	"Elko, Nevada",
      	"Jackpot, Nevada",
	];
	var availableTags_ETS = [
      	"Innsbruck, Europe",
		"Linz, Europe",
		"Salzburg, Europe",
		"Wien, Europe",
		"Graz, Europe",
		"Klagenfurt am Worthersee, Europe",
		"Brussel, Europe",
		"Liege, Europe",
		"Brno, Europe",
		"Prague, Europe",
		"Ostrava, Europe",
		"Aalborg, Europe",
		"Kobenhavn, Europe",
		"Odense, Europe",
		"Calais, Europe",
		"Dijon, Europe",
		"Lille, Europe",
		"Lyon, Europe",
		"Metz, Europe",
		"Paris, Europe",
		"Reims, Europe",
		"Strasbourg, Europe",
		"Berline, Europe",
		"Bremen, Europe",
		"Dortmund, Europe",
		"Dresden, Europe",
		"Duisburg, Europe",
		"Düsseldorf, Europe",
		"Erfurt, Europe",
		"Frankfurt, Europe",
		"Hamburg, Europe",
		"Hannover, Europe",
		"Kassel, Europe",
		"Kiel, Europe",
		"Köln, Europe",
		"Leipzig, Europe",
		"Magdeburg, Europe",
		"Mannheim, Europe",
		"München, Europe",
		"Nürnberg, Europe",
		"Osnabrück, Europe",
		"Rostock, Europe",
		"Stuttgart, Europe",
		"Budapest, Europe",
		"Debrecen, Europe",
		"Pécs, Europe",
		"Szeged, Europe",
		"Milano, Europe",
		"Torino, Europe",
		"Verona, Europe",
		"Venezia, Europe",
		"Luxembourg, Europe",
		"Amsterdam, Europe",
		"Groningen, Europe",
		"Rotterdam, Europe",
		"Bergen, Europe",
		"Kristiansand, Europe",
		"Oslo, Europe",
		"Stavanger, Europe",
		"Szczecin, Europe",
		"Poznań, Europe",
		"Wrocław, Europe",
		"Bialystok, Europe",
		"Gdańsk, Europe",
		"Katowice, Europe",
		"Kraków, Europe",
		"Lublin, Europe",
		"Łódź, Europe",
		"Olsztyn, Europe",
		"Warszawa, Europe",
		"Bratislava, Europe",
		"Banská Bystrica, Europe",
		"Košice, Europe",
		"Göteborg, Europe",
		"Helsingborg, Europe",
		"Jönköping, Europe",
		"Kalmar, Europe",
		"Karlskrona, Europe",
		"Linköping, Europe",
		"Malmö, Europe",
		"Örebro, Europe",
		"Stockholm, Europe",
		"Uppsala, Europe",
		"Västerås, Europe",
		"Växjö, Europe",
		"Bern, Europe",
		"Genève, Europe",
		"Zürich, Europe",
		"Birmingham, Europe",
		"Cambridge, Europe",
		"Carlisle, Europe",
		"Dover, Europe",
		"Felixstowe, Europe",
		"Grimsby, Europe",
		"Liverpool, Europe",
		"London, Europe",
		"Manchester, Europe",
		"Newcastle-upon-Tyne, Europe",
		"Plymouth, Europe",
		"Sheffield, Europe",
		"Southampton, Europe",
		"Aberdeen, Europe",
		"Edinburgh, Europe",
		"Glasgow, Europe",
		"Cardiff, Europe",
		"Swansea, Europe"
    ];
    var availableTags = availableTags_ATS.concat(availableTags_ETS);
    previousValue = "";
    $( "#tags_start" ).autocomplete({
      	source: availableTags,
      	autoFocus: true,
      	change: function(event,ui) {
      		// Check if it's the same as the destination
      		if(ui.item.value == $('#tags_finish').val())
      		{
      			alert("The start and end points cannot be the same!");
      			$('#tags_finish,#tags_start').val('');
      		}
      		// if($.inArray(ui.item.value,availableTags_ETS) && $.inArray($('#tags_finish').val(),availableTags_ATS) || $.inArray(ui.item.value,availableTags_ATS) && $.inArray($('#tags_finish').val(),availableTags_ETS) > -1)
      		// {
      		// 	alert("You can't travel between the US and Europe yet!");
      		// 	$('#tags_finish,#tags_start').val('');
      		// }
      	}
    }).keyup(function() {
	    var isValid = false;
	    for (i in availableTags) {
	        if (availableTags[i].toLowerCase().match(this.value.toLowerCase())) {
	            isValid = true;
	        }
	    }
	    if (!isValid) {
	        this.value = previousValue
	    } else {
	        previousValue = this.value;
	    }
	});

    $( "#tags_finish" ).autocomplete({
      source: availableTags,
      autoFocus: true,
      change: function(event,ui) {
      	if(ui.item.value == $('#tags_start').val())
  		{
  			alert("The start and end points cannot be the same!");
  			$('#tags_finish,#tags_start').val('');
  		}
    //   	if($.inArray(ui.item.value,availableTags_ETS) && $.inArray($('#tags_start').val(),availableTags_ATS) || $.inArray(ui.item.value,availableTags_ATS) && $.inArray($('#tags_start').val(),availableTags_ETS) > -1)
  		// {
  		// 	alert("You can't travel between the US and Europe yet!");
  		// 	$('#tags_finish,#tags_start').val('');
  		// }
      }
    }).keyup(function() {
	    var isValid = false;
	    for (i in availableTags) {
	        if (availableTags[i].toLowerCase().match(this.value.toLowerCase())) {
	            isValid = true;
	        }
	    }
	    if (!isValid) {
	        this.value = previousValue
	    } else {
	        previousValue = this.value;
	    }
	});



    function get_elapsed_time_string(total_seconds)
    {
	  function pretty_time_string(num) {
	    return ( num < 10 ? "0" : "" ) + num;
	  }

	  var hours = Math.floor(total_seconds / 3600);
	  total_seconds = total_seconds % 3600;

	  var minutes = Math.floor(total_seconds / 60);
	  total_seconds = total_seconds % 60;

	  var seconds = Math.floor(total_seconds);

	  // Pad the minutes and seconds with leading zeros, if required
	  hours = pretty_time_string(hours);
	  minutes = pretty_time_string(minutes);
	  seconds = pretty_time_string(seconds);

	  // Compose the string for display
	  var currentTimeString = hours + ":" + minutes + ":" + seconds;

	  return currentTimeString;
	}

    var paused = false;
    var leavingWarning = false;

	$('.start-journey').click(function() {

		// Set warnig flag if they try to leave the page in the middle of a job.
		leavingWarning = true;

		console.log("Saving journey details, please wait...");
		console.log('Display leaving warning?: '+leavingWarning);

		// Save job to database before doing anything else.
		$.ajax({
  			method: "POST",
  			url: $('input[name="submission_url"]').val(),
  			data: {
  				startjob:'true',
  				time_added: $('input[name="time_added"]').val(),
  				time_estimate: $('input[name="time_estimate"]').val(),
  				time_taken: $('input[name="time_taken"]').val(),
  				time_taken_seconds: $('input[name="time_taken_seconds"]').val(),
  				start_point: $('#tags_start').val(),
  				end_point: $('#tags_finish').val(),
  				completed: $('input[name="completed"]').val(),
  				cargo: $('input[name="cargo"]').val(),
  				distance: $('input[name="distance"]').val(),
  				notes: $('input[name="notes"]').val(),
  				approved: $('input[name="approved"]').val()
  			}
		})
  		.done(function( msg ) {
  			var lastInsertId;
  			console.log(msg);
			if(msg == "error")
			{
				console.log("Error saving job!");
				alert("There was a problem saving this job, please try again");
				location.reload();
			}
			else if(msg == "no-data")
			{
				console.log("Error saving job! (No data supplied...)");
				alert("There was a problem saving this job, please try again");
				location.reload();
			}
			else if(msg == "no-login")
			{
				console.log("Error saving job! You do not appear to be logged in!");
				alert("Please make sure you are logged in!");
				location.reload();
			}
			else
			{
				$('input[name="journeyID"]').val(msg);
				console.log("Saved!");
			}
  		});

		var elapsed_seconds = 0;
		var tacograph = setInterval(function() {
			if(paused != true)
			{
		  		elapsed_seconds = elapsed_seconds + 1;
		  		$('.timer').text(get_elapsed_time_string(elapsed_seconds));
		  		$('input[name="time_taken_seconds"]').val(elapsed_seconds);
		  	}
		}, 1000);


		$('.start-journey').css('display','none');
		$('.finish-journey').removeClass('hide');
		$('.cancel-journey').removeClass('hide');
	});

	$('.cancel-journey').click(function() {
		location.reload();
	});

	$('.finish-journey').click(function() {
		$('#confirm-job').openModal();
	});
	
	$('.finish-journey').click(function() {
		// Populate table
		$('span.confirm_start_point').html($('#tags_start').val());
		$('span.confirm_end_point').html($('#tags_finish').val());
		$('span.confirm_cargo').html($('input[name="cargo"]').val());
		$('span.confirm_distance').html($('input[name="distance"]').val());
		$('span.confirm_time_taken').html($('.timer-container .timer').text());
		$('input[name="time_taken"]').val(parseInt($('input[name="time_taken_seconds"]').val()));
		$('span.confirm_time_taken_seconds').html($('input[type="time_taken_seconds"]').val());
		//$('span.confirm_time_estimate').html($('input[name="time_estimate"]').val());
		//console.log($('input[name="time_taken_seconds"]').val() + ' <--> '+ $('input[name="time_estimate"]').val());
		if(parseInt($('input[name="time_taken_seconds"]').val()) < parseInt($('input[name="time_estimate"]').val()))
		{
			$('span.in-time').removeClass('hide');
		}
		if(parseInt($('input[name="time_taken_seconds"]').val()) == parseInt($('input[name="time_estimate"]').val()))
		{
			$('span.on-time').removeClass('hide');
		}
		if(parseInt($('input[name="time_taken_seconds"]').val()) > parseInt($('input[name="time_estimate"]').val()))
		{
			$('span.late').removeClass('hide');
		}
	});

	$('form[name="complete_job"]').submit(function() {
		leavingWarning = false;
	});

	window.onbeforeunload = function()
	{
  		if(leavingWarning == true)
  		{
  			return 'If you leave or refresh this page, this job will be ignored!';
		}
	};

	$('.modal-trigger').leanModal();

});